//TODO: need to define the desired interface for child robot models, but at least sets up polymorphic inheritance/late binding
// (not great for hard realtime code, but good for testing library)

#ifndef ROBOTBASE_HPP
#define ROBOTBASE_HPP

class RobotBase
{
protected:
    //protected ctor to prevent direct use
    RobotBase()
    {
        m_num_links = 0;
    }

    RobotBase(const unsigned int num_links) : m_num_links(num_links)
    {}

    unsigned int m_num_links;

    enum class JointType
    {
        rotate,
        rot_x,
        rot_y,
        rot_z,
        translate,
        trans_x,
        trans_y,
        trans_z,
        none
        //helical,
        //free      TODO: are these needed?
    };

    unsigned int GetNumLinks() const
    {
        return m_num_links;
    }
};  //class RobotBase

#endif //ROBOTBASE_HPP