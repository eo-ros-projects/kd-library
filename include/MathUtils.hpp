//Contain general math, transformation, etc. functions
//Evan Ogden

#ifndef MATHUTILS_HPP
#define MATHUTILS_HPP

#include <cmath>

#include <Eigen/Dense>

//NOTE: for Eigen types that are multiples of 16 bytes, see https://eigen.tuxfamily.org/dox/group__TopicStlContainers.html
typedef Eigen::Vector3d Vector3d;
typedef Eigen::Matrix3d Matrix3d;
typedef Eigen::Matrix<double, 6, 1> Vector6d;
typedef Eigen::Matrix<double, 6, 6> Matrix6d;

typedef std::vector<Vector6d, Eigen::aligned_allocator<Vector6d>> VecEigenVector6d;
typedef std::vector<Matrix6d, Eigen::aligned_allocator<Matrix6d>> VecEigenMatrix6d;

namespace MathUtils
{
    //TODO: separate namespace/file for "SpatialUtils"?
    Matrix3d Vec3dToSkew3d(const Vector3d& v)
    {
        Matrix3d v_skew;
        v_skew << 0.0,   -v(2), v(1),
                  v(2),  0.0,   -v(0),
                  -v(1), v(0),  0.0;
        return v_skew;
    }

    //TODO: rewrite these to use Eigen::block(), topLeftCorner<>(), etc.?
    Matrix6d RotationX6D(const double angle)
    {
        double c = cos(angle);
        double s = sin(angle);

        Matrix6d tf_mat;
        tf_mat << 1.0, 0.0, 0.0, 0.0, 0.0,  0.0,
                  0.0, c,   s,   0.0, 0.0,  0.0,
                  0.0, -s,  c,   0.0, 0.0,  0.0,
                  0.0, 0.0, 0.0, 1.0, 0.0,  0.0,
                  0.0, 0.0, 0.0, 0.0, c,    s,
                  0.0, 0.0, 0.0, 0.0, -s,   c;
        return tf_mat;
    }

    Matrix6d RotationY6D(const double angle)
    {
        double c = cos(angle);
        double s = sin(angle);

        Matrix6d tf_mat;
        tf_mat << c,   0.0, -s,  0.0, 0.0, 0.0,
                  0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                  s,   0.0, c,   0.0, 0.0, 0.0,
                  0.0, 0.0, 0.0, c,   0.0, -s,
                  0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
                  0.0, 0.0, 0.0, s,   0.0, c;
        return tf_mat;
    }

    Matrix6d RotationZ6D(const double angle)
    {
        double c = cos(angle);
        double s = sin(angle);

        Matrix6d tf_mat;
        tf_mat << c,   s,   0.0, 0.0, 0.0, 0.0,
                  -s,  c,   0.0, 0.0, 0.0, 0.0,
                  0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                  0.0, 0.0, 0.0, c,   s,   0.0,
                  0.0, 0.0, 0.0, -s,  c,   0.0,
                  0.0, 0.0, 0.0, 0.0, 0.0, 1.0;
        return tf_mat;
    }

    Matrix6d Rotation6D(const Vector3d& axis, const double angle)
    {
        double c = cos(angle);
        double s = sin(angle);

        //TODO: Featherstone uses equivalent 2*sin(theta/2)^2 - why? numerical advantages?
        double omc = 1 - c;
        Matrix3d R = c*Matrix3d::Identity() - s*Vec3dToSkew3d(axis) + omc*(axis*axis.transpose());

        Matrix6d tf_mat = Matrix6d::Zero();
        tf_mat.topLeftCorner<3, 3>() = R; 
        tf_mat.bottomRightCorner<3, 3>() = R; 
        return tf_mat;
    }

    Matrix6d Translation6D(const Vector3d& p)
    {
        Matrix6d tf_mat = Matrix6d::Identity();
        tf_mat.bottomLeftCorner<3, 3>() = Vec3dToSkew3d(p).transpose();
        // tf_mat << 1.0,   0.0,   0.0,   0.0, 0.0, 0.0,
        //           0.0,   1.0,   0.0,   0.0, 0.0, 0.0,
        //           0.0,   0.0,   1.0,   0.0, 0.0, 0.0,
        //           0.0,   p(2),  -p(1), 1.0, 0.0, 0.0,
        //           -p(2), 0.0,   p(0),  0.0, 1.0, 0.0,
        //           p(1),  -p(0), 0.0,   0.0, 0.0, 1.0;
        return tf_mat;
    }

    //TODO: use "pluho" or "plux" from Featherstone? A-to-B or vice-versa?
    Matrix6d CalculatePluckerTFFromHomogenous(const Matrix3d& R, const Vector3d& p)
    {
        Matrix6d plucker_tf;
        plucker_tf.topLeftCorner<3, 3>() = R;
        plucker_tf.topRightCorner<3, 3>() = Matrix3d::Zero();
        plucker_tf.bottomLeftCorner<3, 3>() = Vec3dToSkew3d(p)*R;
        plucker_tf.bottomRightCorner<3, 3>() = R;

        return plucker_tf;
    }

    Matrix6d SpatialCrossMotion(const Vector6d& v)
    {
        Matrix6d v_cross = Matrix6d::Zero();
        v_cross.topLeftCorner<3, 3>() = Vec3dToSkew3d(v.head<3>());
        v_cross.bottomLeftCorner<3, 3>() = Vec3dToSkew3d(v.tail<3>());
        v_cross.bottomRightCorner<3, 3>() = Vec3dToSkew3d(v.head<3>());
        // v_cross << 0.0,   -v(2), v(2),  0.0,   0.0,   0.0,
        //            v(2),  0.0,   -v(0), 0.0,   0.0,   0.0
        //            -v(1), v(0),  0.0,   0.0,   0.0,   0.0,
        //            0.0,   -v(5), v(4),  0.0,   -v(2), v(1),
        //            v(5),  0.0,   -v(3), v(2),  0.0,   -v(0),
        //            -v(4), v(3),  0.0,   -v(1), v(0),  0.0;
        return v_cross;
    }

    Matrix6d SpatialCrossForce(const Vector6d& f)
    {
        Matrix6d f_cross;
        f_cross = -1*SpatialCrossMotion(f).transpose();
        return f_cross;
    }

    //Calculates inertia at arbitrary point, use TranslateSpatialInertia() to shift location
    Matrix6d CalculateSpatialInertia(const Matrix3d& I_p, const Vector3d& pos, const double mass)
    {
        // Matrix3d skew_pos_mat;
        // skew_pos_mat << Vec3dToSkew3d(pos);
        Matrix3d skew_pos_mat = Vec3dToSkew3d(pos);
        Matrix6d spatial_inertia;
        //Assumes inertia already determined at point, no need for parallel axis
        //spatial_inertia.topLeftCorner<3, 3>() = I_p + mass*skew_pos_mat*skew_pos_mat.transpose();
        spatial_inertia.topLeftCorner<3, 3>() = I_p;
        spatial_inertia.topRightCorner<3, 3>() = mass*skew_pos_mat;
        spatial_inertia.bottomLeftCorner<3, 3>() = mass*skew_pos_mat.transpose();
        spatial_inertia.bottomRightCorner<3, 3>() = mass*Matrix3d::Identity();

        return spatial_inertia;
    }

    //TODO: not sure this is correct
    Matrix6d TransformSpatialInertia(const Matrix6d& spatial_inertia_A, const Matrix6d& spatial_tf_A_to_B)
    {
        Matrix6d spatial_inertia_B;
        spatial_inertia_B << spatial_tf_A_to_B.transpose()*spatial_inertia_A*spatial_tf_A_to_B;
        return spatial_inertia_B;
    }

    //TODO: not sure this is correct
    Matrix6d TranslateSpatialInertia(const Matrix6d& spatial_inertia_A, const Vector3d& displ)
    {
        Matrix6d spatial_tf;
        spatial_tf = Matrix6d::Identity();
        spatial_tf.bottomLeftCorner<3, 3>() = Vec3dToSkew3d(displ).transpose();
        Matrix6d spatial_inertia_B = TransformSpatialInertia(spatial_inertia_A, spatial_tf);
        return spatial_inertia_B;
    }
}

#endif // MATHUTILS_HPP