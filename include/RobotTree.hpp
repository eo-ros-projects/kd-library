//Note: functions based on MATLAB code by R. Featherstone, http://royfeatherstone.org/spatial/v2/
//and Algorithm 3.2 or Springer Handbook of Robotics, 2nd ed

#ifndef ROBOTTREE_HPP
#define ROBOTTREE_HPP

#include <array>
#include <cassert>
#include <iostream>
//#include <memory>
#include <vector>
#include <utility>

#include <Eigen/Dense>
#include <kdl/tree.hpp>
#include <kdl_parser/kdl_parser.hpp>

#include "MathUtils.hpp"
#include "RobotBase.hpp"

//typedef std::shared_ptr<RobotTree::RobotTree> RobotTree_sptr;

//TODO: any case where # links != # joints?
class RobotTree : RobotBase
{
public:
    //Note: using std::vector instead of array to keep library generic, number of joints
    //(thus, memory size) not known at compile time

    //dyn
    std::vector<double> m_joint_forces_cmd;
    //VecEigenVector6d m_ext_forces_cmd;

    //model
    std::vector<std::string> m_segment_names;
    std::vector<int> m_link_parents;           //p(i)      //TODO: change this to vector of iterators?
    std::vector<JointType> m_joint_types;
    VecEigenVector6d m_joint_mobilities;       //Phi
    VecEigenMatrix6d m_link_geom_tfs;          //X_L(i)
    VecEigenMatrix6d m_joint_to_joint_tfs;     //(^i)X_p(i)
    VecEigenMatrix6d m_total_tfs;              //(^i)X_0
    VecEigenMatrix6d m_spatial_inertias;       //I_i

    Vector6d m_spatial_grav_accel;
    //TODO: should be Vector6d(0.0, 0.0, 0.0, 0.0, 0.0, -9.806) (TODO: check sign), but
    //adding default values for Eigen object caused compiler errors

    RobotTree(); //For MyCuteGazeboInterface, which declares it without initalization

    RobotTree(const int num_links, const Vector6d& accel_grav);
    RobotTree(const KDL::Tree& tree, const Vector6d& accel_grav);

    void SetMeasuredJointPositions(const std::vector<double>& meas_pos);
    void SetMeasuredJointVelocities(const std::vector<double>& meas_vel);
    void SetMeasuredJointAccelerations(const std::vector<double>& meas_accel);
    void SetMeasuredJointForces(const std::vector<double>& meas_effort);
    void SetMeasuredExtForces(const std::vector<std::array<double, 6>>& meas_forces);

    ~RobotTree(){}

private:
    //TODO: read robot parameters in from file (URDF, or hand-written yaml?)
    //TODO: if robot model from URDF isn't held in specific order, RNEA/similar
    // may not work - add sort method that arranges all m_num_links vectors so that
    // m_link_parents makes the recursive methods work?
    //TODO: measurement processing (filtering, re-orienting external forces to ground frame)
    //TODO: use mutexs/similar to isolate measurement update and use? example: lock m_joint_pos when receiving
    //subscribed data; what about inside ID calc? any issue with inconsistent/changing values? If fast enough,
    //probably no issue, but may be needed for slow loop rates

    //kinem
    //TODO: this assumes joints are 1D, how to incorporate mobile bases? 6x1D joints or vector-of-vectors?
    std::vector<double> m_joint_pos_meas;    //TODO: should 1D items be std::vector(), or Eigen?
    std::vector<double> m_joint_vel_meas;
    std::vector<double> m_joint_accel_meas;
    std::vector<double> m_joint_pos_cmd;
    std::vector<double> m_joint_vel_cmd;
    std::vector<double> m_joint_accel_cmd;

    //TODO: segment/end-effector pos/vel/accel (spatial? cartesian? operational space?)?
    //Or only store it once translated to joint space?

    //dyn
    std::vector<double> m_joint_forces_meas;
    VecEigenVector6d m_ext_forces_meas;

    void InitialAllocate();

    void ImportKDLInfo(const KDL::Tree& tree);

    JointType ConvertKDLJointType(const KDL::Joint& joint);
    Vector6d GetJointMobility(const JointType& jt, const Vector3d& axis);
    Matrix6d CalculateJointTransform(const JointType& jt, const Vector6d& jmob, const double jpos);

    std::vector<double> ID_RNEA();
};  //class RobotTree

#endif //ROBOTTREE_HPP