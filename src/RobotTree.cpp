//Note: functions based on MATLAB code by R. Featherstone, http://royfeatherstone.org/spatial/v2/
//and Algorithm 3.2 or Springer Handbook of Robotics, 2nd ed

#include "RobotTree.hpp"

RobotTree::RobotTree()
{
    //does nothing, but needed for un-initialized members of other classes (ex., MyCuteGazeboInterface)
    //TODO: copy constructor needed?
}

//TODO: any case where # links != # joints?
RobotTree::RobotTree(const int num_links, const Vector6d& accel_grav) :
    RobotBase::RobotBase(num_links), m_spatial_grav_accel(accel_grav)
{
    InitialAllocate();

    //TODO: set values for base of tree (joint_to_joint_tfs = I, etc.)
}

RobotTree::RobotTree(const KDL::Tree& tree, const Vector6d& accel_grav) :
    RobotBase::RobotBase(tree.getNrOfSegments()), m_spatial_grav_accel(accel_grav)
{
    //Note:: "world" dummy link has no info, so drop from calculations; m_num_links already accounts for it
    //TODO: make sure this is OK; if not, increment m_num_Links and adjust ImportKDLInfo (may be
    //OK for grounded links, but not mobile robots?)
    InitialAllocate();
    ImportKDLInfo(tree);
}

//TODO: import other robot parameters in from file (hand-written yaml?)
//TODO: measurement processing (filtering, re-orienting external forces to ground frame)

void RobotTree::SetMeasuredJointPositions(const std::vector<double>& meas_pos)
{
    if(meas_pos.size() == m_num_links)   //inherently checks if empty//size == 0
        //TODO: smart pointers?
        //TODO: where to put filtering? should robot model know about it, or leave that to the interface?
        m_joint_pos_meas = meas_pos;
    else
        //TODO: ROS_ERROR message, don't update measurements
        0;
}

void RobotTree::SetMeasuredJointVelocities(const std::vector<double>& meas_vel)
{
    if(meas_vel.size() == m_num_links)
        //TODO: smart pointers?
        m_joint_vel_meas = meas_vel;
    else
        //TODO: rate limited ROS message?
        //Note: DON'T calculate it manually from position data here, leave that up to the interface
        //Robot model shouldn't have to know loop rate, manage measurement connections
        std::fill(m_joint_vel_meas.begin(), m_joint_vel_meas.end(), 0.0);
}

void RobotTree::SetMeasuredJointAccelerations(const std::vector<double>& meas_accel)
{
    if(meas_accel.size() == m_num_links)
        //TODO: smart pointers?
        m_joint_accel_meas = meas_accel;
    else
        //TODO: rate limited ROS message?
        //Note: DON'T calculate it manually from position data here, leave that up to the interface
        //Robot model shouldn't have to know loop rate, manage measurement connections
        std::fill(m_joint_accel_meas.begin(), m_joint_accel_meas.end(), 0.0);
}

void RobotTree::SetMeasuredJointForces(const std::vector<double>& meas_effort)
{
    if(meas_effort.size() == m_num_links)
        //TODO: smart pointers?
        m_joint_forces_meas = meas_effort;
    else
        //TODO: rate limited ROS message?
        std::fill(m_joint_forces_meas.begin(), m_joint_forces_meas.end(), 0.0);
}

//TODO: these need to be put into different frames, other transformations
void RobotTree::SetMeasuredExtForces(const std::vector<std::array<double, 6>>& meas_forces)
{
    if(meas_forces.size() == m_num_links)
    {
        //TODO: smart pointers?
        for(size_t i = 0; i < m_num_links; ++i)
        {
            for(size_t j = 0; j < 6; ++j)
                m_ext_forces_meas.at(i)(j) = meas_forces.at(i).at(j);
        }
    }
    else
    {
        //TODO: rate limited ROS message?
        std::fill(m_ext_forces_meas.begin(), m_ext_forces_meas.end(), Vector6d::Zero());
    }
        
}

void RobotTree::InitialAllocate()
{
    //TODO: for better real-time safety, use define/constexpr for num_links?
    //Only useful for known robot geom
    m_segment_names.reserve(m_num_links);
    m_joint_types.reserve(m_num_links);
    m_joint_mobilities.reserve(m_num_links);
    m_joint_to_joint_tfs.reserve(m_num_links);
    m_link_geom_tfs.reserve(m_num_links);
    m_link_parents.reserve(m_num_links);
    m_total_tfs.reserve(m_num_links);
    m_spatial_inertias.reserve(m_num_links);

    //Initialize measurements, in case measurements are never called
    std::fill(m_joint_pos_meas.begin(), m_joint_pos_meas.end(), 0.0);
    std::fill(m_joint_vel_meas.begin(), m_joint_vel_meas.end(), 0.0);
    std::fill(m_joint_accel_meas.begin(), m_joint_accel_meas.end(), 0.0);
    std::fill(m_joint_forces_meas.begin(), m_joint_forces_meas.end(), 0.0);
    std::fill(m_ext_forces_meas.begin(), m_ext_forces_meas.end(), Vector6d::Zero());
}

void RobotTree::ImportKDLInfo(const KDL::Tree& tree)
{
    KDL::SegmentMap segments_to_import = tree.getSegments();
    //TODO: hardcoded, is this OK? maybe import 'world_name' from YAML as part of the model import process?
    std::string world_segment_name = "world";

    for(auto segment_i : segments_to_import)
    {
        if(segment_i.first != world_segment_name)
            m_segment_names.push_back(segment_i.first);
    }

    for(auto segment_iter : segments_to_import)
    {
        if(segment_iter.first == world_segment_name)
            continue;

        std::string curr_parent_name = GetTreeElementParent(segment_iter.second)->first;
        std::vector<std::string>::iterator parent_name_iter = std::find(m_segment_names.begin(), 
                                                            m_segment_names.end(), curr_parent_name);
        if(parent_name_iter != m_segment_names.end())
            m_link_parents.push_back(std::distance(m_segment_names.begin(), parent_name_iter));
        else
            m_link_parents.push_back(-1);

        KDL::Segment curr_segment = GetTreeElementSegment(segment_iter.second);
        m_joint_types.push_back(ConvertKDLJointType(curr_segment.getJoint()));
        Vector3d curr_joint_axis = Eigen::Map<Eigen::Matrix<double, 3, 1, Eigen::ColMajor>>(
                                                curr_segment.getJoint().JointAxis().data);
        m_joint_mobilities.push_back(GetJointMobility(m_joint_types.back(), curr_joint_axis));

        //init to dummy values, better than uninitialized...
        m_joint_to_joint_tfs.push_back(CalculateJointTransform(m_joint_types.back(), m_joint_mobilities.back(), 0.0));
        m_total_tfs.push_back(Matrix6d::Identity());

        //TODO: make sure these are the correct direction (distal vs proximal)
        KDL::Frame curr_frame = curr_segment.getFrameToTip();
        //TODO: use typedefs in Map<>()?
        Matrix3d curr_frame_rot = Eigen::Map<Eigen::Matrix<double, 3, 3, Eigen::RowMajor>>(curr_frame.M.data);
        Vector3d curr_frame_pos = Eigen::Map<Eigen::Matrix<double, 3, 1, Eigen::ColMajor>>(curr_frame.p.data);
        m_link_geom_tfs.push_back(MathUtils::CalculatePluckerTFFromHomogenous(curr_frame_rot, curr_frame_pos));

        KDL::RigidBodyInertia curr_inertia = curr_segment.getInertia();
        Matrix3d curr_inertia_at_origin = Eigen::Map<Eigen::Matrix<double, 3, 3, Eigen::RowMajor>>(
                                                        curr_inertia.getRotationalInertia().data);
        Vector3d com_position = Eigen::Map<Eigen::Matrix<double, 3, 1, Eigen::ColMajor>>(
                                                        curr_inertia.getCOG().data);
        Matrix6d spatial_inertia_at_origin = MathUtils::CalculateSpatialInertia(curr_inertia_at_origin, 
                                                    com_position, curr_inertia.getMass());
        m_spatial_inertias.push_back(MathUtils::TranslateSpatialInertia(spatial_inertia_at_origin, -com_position));
    }

    //TODO: map sorted by keys (string -> alphabetical), maybe re-sort as proximal-to-distal? Could be
    //sorted according to ROS joint message format (to avoid extra reorganizing step)
}

RobotBase::JointType RobotTree::ConvertKDLJointType(const KDL::Joint& joint)
{
    //TODO: maybe use std::map instead? more memory, but may be faster
    switch(joint.getType())
    {
        case KDL::Joint::RotAxis:
            return JointType::rotate;
            break;
        case KDL::Joint::RotX:
            return JointType::rot_x;
            break;
        case KDL::Joint::RotY:
            return JointType::rot_y;
            break;
        case KDL::Joint::RotZ:
            return JointType::rot_z;
            break;
        case KDL::Joint::TransAxis:
            return JointType::translate;
            break;
        case KDL::Joint::TransX:
            return JointType::trans_x;
            break;
        case KDL::Joint::TransY:
            return JointType::trans_y;
            break;
        case KDL::Joint::TransZ:
            return JointType::trans_z;
            break;
        case KDL::Joint::None:
            return JointType::none;
            break;
        default:
            //TODO: error handling? use error code, make current output an out param?
            std::cerr << "Unhandled KDL::Joint type in ConvertKDLJointType" << std::endl;
            break;
    }
}

Vector6d RobotTree::GetJointMobility(const JointType& jt, const Vector3d& axis)
{
    Vector6d joint_mobility;
    Vector3d normed_axis = axis.normalized();
    switch(jt)
    {
        case JointType::rotate:
            joint_mobility << Vector6d::Zero();
            joint_mobility.head<3>() = normed_axis;
            break;
        case JointType::rot_x:
            joint_mobility << 1.0, 0.0, 0.0, 0.0, 0.0, 0.0;
            break;
        case JointType::rot_y:
            joint_mobility << 0.0, 1.0, 0.0, 0.0, 0.0, 0.0;
            break;
        case JointType::rot_z:
            joint_mobility << 0.0, 0.0, 1.0, 0.0, 0.0, 0.0;
            break;
        case JointType::translate:
            joint_mobility << Vector6d::Zero();
            joint_mobility.tail<3>() = normed_axis;
            break;
        case JointType::trans_x:
            joint_mobility << 0.0, 0.0, 0.0, 1.0, 0.0, 0.0;
            break;
        case JointType::trans_y:
            joint_mobility << 0.0, 0.0, 0.0, 0.0, 1.0, 0.0;
            break;
        case JointType::trans_z:
            joint_mobility << 0.0, 0.0, 0.0, 0.0, 0.0, 1.0;
            break;
        case JointType::none:
            joint_mobility = Vector6d::Zero();
            break;
        default:
            //TODO: error handling? use error code, make current output an out param?
            std::cerr << "Unknown JointType in GetJointMobility" << std::endl;
            break;
    }

    return joint_mobility;
}

//TODO: change this to using index/iterator? (calculate it for certain link in tree, not certain type of joint -
// moves indexing/access inside the function)
//TODO: duplicated logic with predefined behaviors for pure x/y/z axis joints
Matrix6d RobotTree::CalculateJointTransform(const JointType& jt, const Vector6d& jmob, const double jpos)
{
    Matrix6d joint_transform;
    Vector3d axis;  // as opposed to braces to resolve 'jumpt to case label' error
    switch(jt)
    {
        case JointType::rotate:
            axis = jmob.head<3>();
            joint_transform = MathUtils::Rotation6D(axis, jpos);
            break;
        case JointType::rot_x:
            joint_transform = MathUtils::RotationX6D(jpos);
            break;
        case JointType::rot_y:
            joint_transform = MathUtils::RotationY6D(jpos);
            break;
        case JointType::rot_z:
            joint_transform = MathUtils::RotationZ6D(jpos);
            break;
        case JointType::translate:
            axis = jmob.tail<3>();
            joint_transform = MathUtils::Translation6D(jpos*axis);
            break;
        case JointType::trans_x:
            joint_transform = MathUtils::Translation6D(Vector3d(jpos, 0.0, 0.0));
            break;
        case JointType::trans_y:
            joint_transform = MathUtils::Translation6D(Vector3d(0.0, jpos, 0.0));
            break;
        case JointType::trans_z:
            joint_transform = MathUtils::Translation6D(Vector3d(0.0, 0.0, jpos));
            break;
        case JointType::none:
            joint_transform = Matrix6d::Identity();
            break;
        default:
            //TODO: error handling? use error code, make current output an out param?
            std::cerr << "Unknown JointType in CalculateJointTransform" << std::endl;
            break;
    }

    return joint_transform;
}

//TODO: make sure ext_forces are expressed in ground/link-0 coordinates
std::vector<double> RobotTree::ID_RNEA()
{
    //TODO: what impact do these assertions have on runtime?
    //TODO: move these somewhere else (measurement read-in)
    assert(m_num_links == m_joint_pos_meas.size());
    assert(m_num_links == m_joint_vel_meas.size());
    assert(m_num_links == m_joint_accel_meas.size());
    assert(m_num_links == m_ext_forces_meas.size());

    VecEigenVector6d spatial_vels;
    VecEigenVector6d spatial_accels;
    VecEigenVector6d spatial_forces;

    spatial_vels.reserve(m_num_links);
    spatial_accels.reserve(m_num_links);
    spatial_forces.reserve(m_num_links);

    for(unsigned int i = 0; i < m_num_links; ++i)
    {
        Matrix6d curr_joint_tf = CalculateJointTransform(m_joint_types.at(i), m_joint_mobilities.at(i), m_joint_pos_meas.at(i));
        Vector6d curr_joint_vel = m_joint_mobilities.at(i)*m_joint_vel_meas.at(i);
        m_joint_to_joint_tfs.at(i) = curr_joint_tf*m_link_geom_tfs.at(i);
        if(m_link_parents.at(i) < 0)
        {
            spatial_vels.at(i) = curr_joint_vel;
            spatial_accels.at(i) = m_joint_to_joint_tfs.at(i)*m_spatial_grav_accel +
                    m_joint_mobilities.at(i)*m_joint_accel_meas.at(i);
        }
        else
        {
            spatial_vels.at(i) = m_joint_to_joint_tfs.at(i)*spatial_vels.at(m_link_parents.at(i)) + curr_joint_vel;
            spatial_accels.at(i) = m_joint_to_joint_tfs.at(i)*spatial_accels.at(m_link_parents.at(i)) +
                    m_joint_mobilities.at(i)*m_joint_accel_meas.at(i) +
                    MathUtils::SpatialCrossMotion(spatial_vels.at(i))*curr_joint_vel;
        }

        if(m_link_parents.at(i) >= 0)
            m_total_tfs.at(i) = m_joint_to_joint_tfs.at(i)*m_total_tfs[m_link_parents.at(i)];
        else
            m_total_tfs.at(i) = m_joint_to_joint_tfs.at(i);

        spatial_forces.at(i) = m_spatial_inertias.at(i)*spatial_accels.at(i) +
                MathUtils::SpatialCrossForce(spatial_vels.at(i))*m_spatial_inertias.at(i)*spatial_vels.at(i) -
                m_total_tfs.at(i).transpose().colPivHouseholderQr().solve(m_ext_forces_meas.at(i));
            //Using QR decomp instead of inverse: b = X^T*q <-> q = X^-T*b; TODO: may want
            //to try other methods, like HouseholdeQR (faster) or JacobiSVD (most accurate)
    }

    std::vector<double> ID_joint_forces;
    ID_joint_forces.reserve(m_num_links);

    for(int j = m_num_links-1; j >= 0; --j)
    {
        ID_joint_forces.at(j) = m_joint_mobilities.at(j).transpose()*spatial_forces.at(j);
        if(m_link_parents.at(j) != 0)
        {
            spatial_forces.at(m_link_parents.at(j)) = spatial_forces.at(m_link_parents.at(j)).eval() +
                                        m_joint_to_joint_tfs.at(j).transpose()*spatial_forces.at(j);
        }
    }

    return ID_joint_forces;
}