// Kinematics implementation for single chain robot manipulators.
// Intending to use spatial vectors (Featherstone) internally, connect to
// more common ROS interface using tf2_eigen.
// Evan Ogden

#include <array>
#include <iostream>
#include <string>

#include <Eigen/Dense>
#include <kdl/tree.hpp>
#include <ros/ros.h>

#include "ImportURDF.hpp"
#include "MathUtils.hpp"
#include "RobotTree.hpp"

typedef Eigen::Matrix3d Mat3d;

int main(int argc, char *argv[])
{
    std::cout << "Hello, world!" << std::endl;

    constexpr int num_matrices = 5;
    std::array<Mat3d, num_matrices> matrix_set;

    for(size_t i = 0; i < num_matrices; ++i)
    {
        //Note: had bug where I tried to assign n+1th entry, and it let me
        //for a bit. Then, when printing, it threw a 'stack smashing detected'
        //error and aborted after printing the the nth entry - did std::array
        //not protect against out-of-bounds assignment and range-based for see
        //something was off?
        matrix_set[i] = (i+1)*Mat3d::Identity();
    }

    Eigen::IOFormat format1(Eigen::StreamPrecision, 0, ", ", "\n", "[", "]");

    for(auto i : matrix_set)
    {
        std::cout << i.format(format1) << "\n" << std::endl;
    }

    //See if spatial vectors/matrices need appropriate allocator additions (multiple of 16 bytes)
    Vector6d temp_size_vec6;
    Matrix6d temp_siz_mat6;
    std::cout << "Size of Vector6d: " << sizeof(temp_size_vec6) << "\n";
    std::cout << "Size of Matrix6d: " << sizeof(temp_siz_mat6) << std::endl;

    //Test RobotTree creation from KDL

    std::string lump_robot_descrip;
    std::string robot_desc_name("/robot_description");
    KDL::Tree kdl_tree;

    //Note: giving this a node name allows for error-less URDF import, may be best to just make things nodes
    //unless they're pure libraries
    ros::init(argc, argv, "my_urdf_import_node");

    //TODO: just holding the file for now, need to create similar launch file
    //urdf::Model::initFile("/home/e/ros/catkin_ws/src/cute_robot/cute_model/robots/cyte_300es.urdf.xacro");

    if(ImportModel::ImportParamToKDL(robot_desc_name, kdl_tree))
    {
        //ImportModel::ListModelAttributes(kdl_tree);

        Vector6d temp_grav;
        temp_grav << 0.0, 0.0, 0.0, 0.0, 0.0, -9.806;
        RobotTree cute_tree(kdl_tree, temp_grav);

        std::cout << "RobotTree object created successfully, testing access..." << "\n";

        std::string test_name = "part1";
        std::vector<std::string>::iterator test_name_iter = std::find(cute_tree.m_segment_names.begin(), 
                                                            cute_tree.m_segment_names.end(), test_name);
        int test_index = std::distance(cute_tree.m_segment_names.begin(), test_name_iter);
        std::cout << test_name << " spatial inertia: \n" << cute_tree.m_spatial_inertias.at(test_index) << std::endl;

        //TODO: model created successfully - now, hook up the ROS messages
        //Maybe create container class for RobotTree (inheret from 'Robot' TODO) and message interface, translate messages and
        //update RobotTree state
        //For gazebo sim: minimum messages are cute_arm_controller/command and cute_arm_controller/state
    }

    return 0;
}
